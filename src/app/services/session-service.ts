import { EmployeeModel } from './../models/employee.model';
import { Injectable } from '@angular/core';

@Injectable()
export class SessionService {
    selectedEmployee: EmployeeModel;
    employees: EmployeeModel[];
    constructor() {
      this.selectedEmployee = new EmployeeModel();
      this.employees = [];
    }
}
