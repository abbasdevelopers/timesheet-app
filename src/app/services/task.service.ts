import { EmployeeTaskModel } from './../models/employee-task.model';
import { EmployeeModel } from '../models/employee.model';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment.prod';
import { HttpClient } from '@angular/common/http';
import { EmployeeWorkModel } from '../models/employee-work-model';

@Injectable()
export class TaskServiceService {
    private baseapi = environment.apiUrl;
    constructor(private http: HttpClient) { }

    getalltasks() {
        return this.http.get<EmployeeTaskModel[]>(this.baseapi + "/task/getall");
    }
}
