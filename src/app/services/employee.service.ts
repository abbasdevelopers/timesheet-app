import { EmployeeModel } from './../models/employee.model';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment.prod';
import { HttpClient } from '@angular/common/http';
import { EmployeeWorkModel } from '../models/employee-work-model';

@Injectable()
export class EmployeeService {
    private baseapi = environment.apiUrl;
    constructor(private http: HttpClient) { }

    getallemployees() {
        return this.http.get<EmployeeModel[]>(this.baseapi + "/employee/getall");
    }
    getEmployeeWork(empId: number) {
      return this.http.get<EmployeeWorkModel[]>(this.baseapi + "/employee/work/get/" + empId);
  }
  addEmployeeWork(employeeWrok: EmployeeWorkModel) {
    return this.http.post<EmployeeWorkModel>(this.baseapi + "/employee/work/add", employeeWrok);
}
}
