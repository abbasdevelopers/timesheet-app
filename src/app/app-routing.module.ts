import { TaskAddComponent } from './task-add/task-add.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { TimeSheetComponent } from './time-sheet/time-sheet.component';
import { EmployeeListComponent } from './employee/employee.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {path: '', component: EmployeeListComponent},
  {path: 'timesheet', component: TimeSheetComponent},
  {path: 'task/add', component: TaskAddComponent},
  {path: '**', component: PageNotFoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {onSameUrlNavigation: 'reload'})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
