import { EmployeeWorkModel } from './../models/employee-work-model';
import { SessionService } from './../services/session-service';
import { EmployeeModel } from './../models/employee.model';
import { Component, OnInit, ChangeDetectionStrategy, ViewChild, ElementRef } from '@angular/core';
import { EmployeeService } from '../services/employee.service';
import { Observable } from 'rxjs';
import {map} from 'rxjs/operators';
import { Router } from '@angular/router';

@Component({
  selector: 'app-time-sheet',
  templateUrl: './time-sheet.component.html',
  styleUrls: ['./time-sheet.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TimeSheetComponent implements OnInit {

  @ViewChild('ddlEmployees') ddlEmployees: ElementRef;
  selectedEmployee: EmployeeModel;
  employees: EmployeeModel[];
  tasks: Observable<EmployeeWorkModel[]>;
  sundayTotal: number = 0;
  mondayTotal: number = 0;
  tuesdayTotal: number = 0;
  wednesdayTotal: number = 0;
  thursdayTotal: number = 0;
  fridayTotal: number = 0;
  saturdayTotal: number = 0;
  tasksArray: EmployeeWorkModel[];

  constructor(private sessionService: SessionService,
    private employeeService: EmployeeService,
    private router: Router) { }

  ngOnInit() {
    this.employees = this.sessionService.employees;
    this.selectedEmployee = this.sessionService.selectedEmployee;

    this.loadTasks(this.selectedEmployee.id);
  }
  onEmployeeChange(ddlEmployees: HTMLSelectElement) {
    this.mondayTotal = 0;
    this.tuesdayTotal = 0;
    this.wednesdayTotal = 0;
    this.thursdayTotal = 0;
    this.fridayTotal = 0;
    this.saturdayTotal = 0;
    this.sundayTotal = 0;
    this.loadTasks(+ddlEmployees.value);
  }
  navigateToEmployees() {
    this.router.navigate(['']);
  }
  navigateToAddTask() {
    this.router.navigate(['task/add']);
  }

  private loadTasks(empId: number) {
    this.tasks = this.employeeService.getEmployeeWork(empId).pipe(
      map((data: EmployeeWorkModel[]) => {
        this.calculateTotalWork(data);
        return data;
      })
    );
  }
  private calculateTotalWork(data: EmployeeWorkModel[]) {
    if(data !== null
      && data !== undefined
      && data.length > 0) {
        for (let i = 0; i < data.length; i++) {
          this.sundayTotal = this.sundayTotal + data[i].sunday;
          this.mondayTotal = this.sundayTotal + data[i].monday;
          this.tuesdayTotal = this.sundayTotal + data[i].tuesday;
          this.wednesdayTotal = this.sundayTotal + data[i].wednesday;
          this.thursdayTotal = this.sundayTotal + data[i].thursday;
          this.fridayTotal = this.sundayTotal + data[i].friday;
          this.saturdayTotal = this.sundayTotal + data[i].saturday;
        }
      }
  }

}
