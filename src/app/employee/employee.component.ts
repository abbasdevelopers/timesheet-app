import { Observable } from 'rxjs';
import { SessionService } from './../services/session-service';
import { EmployeeModel } from './../models/employee.model';
import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '../services/employee.service';
import { map } from 'rxjs/operators';

@Component({
    selector: 'employee-list',
    templateUrl: 'employee.component.html',
    styleUrls:['./employee.component.scss']
})

export class EmployeeListComponent implements OnInit {
  employees: Observable<EmployeeModel[]>;
    constructor(private employeeService: EmployeeService,
      private sessionService: SessionService) { }

    ngOnInit() {
      this.employees = this.employeeService.getallemployees().pipe(
        map((data: EmployeeModel[]) => {
          this.sessionService.employees = data;
          console.log(this.sessionService.employees);
          return data;
        })
      );
    }
    setEmployeeInSession(item) {
      this.sessionService.selectedEmployee = item;
    }
}
