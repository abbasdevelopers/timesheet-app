import { EmployeeTaskModel } from './employee-task.model';
export class EmployeeWorkModel {
  taskId = -1;
  employeeId = -1;
  sunday = -1;
  monday = -1;
  tuesday = -1;
  wednesday = -1;
  thursday = -1;
  friday = -1;
  saturday = -1;
  employeeTask: EmployeeTaskModel = new EmployeeTaskModel();
}
