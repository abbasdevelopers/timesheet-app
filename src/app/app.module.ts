import { TaskServiceService } from './services/task.service';
import { SessionService } from './services/session-service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { EmployeeListComponent } from './employee/employee.component';
import { EmployeeService } from './services/employee.service';
import { TimeSheetComponent } from './time-sheet/time-sheet.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { AppRoutingModule } from './app-routing.module';
import { TaskAddComponent } from './task-add/task-add.component';

import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    EmployeeListComponent,
    TimeSheetComponent,
    PageNotFoundComponent,
    TaskAddComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [
    EmployeeService,
    SessionService,
    TaskServiceService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
