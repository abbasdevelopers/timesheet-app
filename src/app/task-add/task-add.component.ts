import { EmployeeWorkModel } from './../models/employee-work-model';
import { EmployeeTaskModel } from './../models/employee-task.model';
import { Router } from '@angular/router';
import { EmployeeService } from './../services/employee.service';
import { SessionService } from './../services/session-service';
import { TaskServiceService } from './../services/task.service';
import { Component, OnInit, ChangeDetectionStrategy, ViewChild } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-task-add',
  templateUrl: './task-add.component.html',
  styleUrls: ['./task-add.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TaskAddComponent implements OnInit {

  constructor(private taskService: TaskServiceService,
    private sessionService: SessionService,
    private employeService: EmployeeService,
    private router: Router) { }

    tasks: Observable<EmployeeTaskModel[]>;
    @ViewChild('f') taskForm: NgForm;
  ngOnInit() {
    this.tasks = this.taskService.getalltasks();
  }
  gotoBack() {
    this.router.navigate(['timesheet']);
  }
  onSubmit() {
    console.log('called');
    const employeeWorkModel: EmployeeWorkModel = {
      taskId: +this.taskForm.value.ddlTasks,
      employeeId: this.sessionService.selectedEmployee.id,
      sunday: this.taskForm.value.txtSunday,
      monday: this.taskForm.value.txtMonday,
      tuesday: this.taskForm.value.txtTuesday,
      wednesday: this.taskForm.value.txtWednesday,
      thursday: this.taskForm.value.txtThursday,
      friday: this.taskForm.value.txtFriday,
      saturday: this.taskForm.value.txtSaturday,
      employeeTask: null
    }
    this.employeService.addEmployeeWork(employeeWorkModel).subscribe(
      (data: EmployeeWorkModel) => {
        this.router.navigate(['timesheet']);
      }
    )
  }
}
